package Assignments;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;
import static java.util.Comparator.comparing;

public class CarText {

	public static void main(String[] args) throws FileNotFoundException {
		                  
		String[] myfile = new String[100];
		int i=0;
		Scanner in = new Scanner(System.in);
		System.out.println("What is the filename? Type the path correctly. example: /Users/firatkaymaz/Desktop/Cars.txt");
		String filename = in.nextLine();
		File file = new File(filename);
		Scanner inputFile = new Scanner(file);
		
		while(inputFile.hasNext()) {
			myfile[i]=inputFile.nextLine();
			i++;
			}
		//System.out.println(myfile[5]);
		
		String[] make = myfile[4].split(" ");
		String[] model = myfile[5].split(" ");
		String[] date = myfile[6].split(" ");
		String[] license = myfile[7].split(" ");
		
		String[] make2 = myfile[10].split(" ");
		String[] model2 = myfile[11].split(" ");
		String[] date2 = myfile[12].split(" ");
		String[] license2 = myfile[13].split(" ");
		
		String[] make3 = myfile[16].split(" ");
		String[] model3 = myfile[17].split(" ");
		String[] date3 = myfile[18].split(" ");
		String[] license3 = myfile[19].split(" ");
		
		String[] make4 = myfile[22].split(" ");
		String[] model4 = myfile[23].split(" ");
		String[] date4 = myfile[24].split(" ");
		String[] license4 = myfile[25].split(" ");
		
		String[] make5 = myfile[28].split(" ");
		String[] model5 = myfile[29].split(" ");
		String[] date5 = myfile[30].split(" ");
		String[] license5 = myfile[31].split(" ");
		
		
		Car car1= new Car();
		car1.setMake(make[1].toString());
		car1.setModel(model[1].toString());
		car1.setYear(Integer.parseInt(date[1]));
		car1.SetLicense(license[1].toString());
		
		//System.out.println(car1.getMake());
		
		Car car2=new Car();
		car2.setMake(make2[1].toString());
		car2.setModel(model2[1].toString());
		car2.setYear(Integer.parseInt(date2[1]));
		car2.SetLicense(license2[1].toString());
		
		//System.out.println(car2.getMake());
		
		Car car3=new Car();
		car3.setMake(make3[1].toString());
		car3.setModel(model3[1].toString());
		car3.setYear(Integer.parseInt(date3[1]));
		car3.SetLicense(license3[1].toString());
		
		//System.out.println(car3.getMake());
		
		Car car4=new Car();
		car4.setMake(make4[1].toString());
		car4.setModel(model4[1].toString());
		car4.setYear(Integer.parseInt(date4[1]));
		car4.SetLicense(license4[1].toString());
		
		//System.out.println(car4.getMake());
		
		Car car5=new Car();
		car5.setMake(make5[1].toString());
		car5.setModel(model5[1].toString());
		car5.setYear(Integer.parseInt(date5[1]));
		car5.SetLicense(license5[1].toString());
		
		//System.out.println(car5.getYear());
		
		ArrayList<Car> mylist = new ArrayList<Car>();
		ArrayList<Car> mylist2 = new ArrayList<Car>();
	
	    mylist.add(car1);
		mylist.add(car2);
		mylist.add(car3);
		mylist.add(car4);
		mylist.add(car5);
		
		//System.out.println(mylist.get(0));
		
		//1985 1980 1993 1966 1996
	
	Collections.sort(mylist, Comparator.comparing(Car::getYear));
	
	for (int a=0; a<mylist.size(); a++) {
		
		System.out.println(mylist.get(a).getYear() +" "+ mylist.get(a).getMake() +" "+ mylist.get(a).getModel() + " "+mylist.get(a).getLicense());
		
	}

	}

}
