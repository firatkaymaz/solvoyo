package Assignments;

public class Car {
	
	String _make;
	String _model;
	int _year;
	String _license;

//Getters	
	
	public String getMake() {
		return _make;
	}
	public String getModel() {
		return _model;
	}
	public int getYear() {
		return _year;
	}
	public String getLicense() {
		return _license;
	}
//Setters	

	public void setMake(String string) {
		this._make=string;
	}
	public void setModel(String model) {
		this._model=model;
	}
	public void setYear(int year) {
		this._year=year;
	}
	public void SetLicense(String license) {
		this._license=license;
		
	}

	
	/*public Car(String name,String make, String model, int year, String license) {
		
		this._name=name;
		this._make=make;
		this._model=model;
		this._year=year;
		this._license=license;
	}*/
	

}
