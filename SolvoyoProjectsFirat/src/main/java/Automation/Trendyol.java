package Automation;


import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Trendyol {
	
	 WebDriver driver;
	 static Random rand = new Random();
	 public static int number = rand.nextInt(9000)+1000;
	 public static int day = rand.nextInt(30 - 1 + 1) + 1;
	 public static int month = rand.nextInt(12 - 1 + 1) + 1;
	 public static int date = rand.nextInt(2010 - 1870 + 1) + 1870;
	 public static int mobile = rand.nextInt(9999999 - 1000000 + 1) + 1000000;
	 public String sday=String.valueOf(day);
	 public String smonth=String.valueOf(month);
	 public String sdate=String.valueOf(date);
	 public String smobile=String.valueOf(mobile);
	 public String str="534"+smobile;
	 public String gender=null; 
	 
	    
		@BeforeTest
		public void SetUp() throws InterruptedException {
			
			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver();
			driver.get("https://www.trendyol.com");
			driver.manage().window().maximize();
			Thread.sleep(2000);
			
		}
		@Test(priority=1, description="Log into Account")
		public void LoginStep () throws InterruptedException{
			
		//WebDriverWait wait = new WebDriverWait(driver,10);	
		//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='homepage-popup']")))); 
		Thread.sleep(1000);
		driver.findElement(By.xpath("//a[@title='Close']")).click();
		Thread.sleep(1000);
		driver.findElement(By.xpath("//div[@class='login-container']//span[@id='not-logged-in-container']")).click();
		//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='q-input-wrapper email-input']//input[@class='q-input']"))));
		Thread.sleep(2000);
		driver.findElement(By.xpath("//div[@class='q-input-wrapper email-input']//input[@class='q-input']")).sendKeys("blackkfredo@gmail.com");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//div[@class='q-input-wrapper']//input[@class='q-input']")).sendKeys("1a2b3c4d");
		Thread.sleep(1000);
		driver.findElement(By.xpath("//button[@type='submit']")).click();
		Thread.sleep(3000);
		driver.findElement(By.xpath("//div[@title='Kapat']")).click();
		//Thread.sleep(2000);
		//driver.findElement(By.xpath("//div[@class='loggedin-panel-container']//div[@class='account-link-container']//a[@href='/Hesabim/KullaniciBilgileri']")).click();
		}
		
		@Test(priority=2, description="Update User Informations")
		public void UpdateUserInformations () throws InterruptedException {
			
			
			Thread.sleep(2000);
			Actions act = new Actions(driver);
			WebElement element=driver.findElement(By.xpath("//div[@class='icon-container']//i[@class='icon navigation-icon-user']"));
			act.moveToElement(element).perform();
			Thread.sleep(1000);
			WebElement elm = driver.findElement(By.xpath("//div[@class='loggedin-panel-container']//div[@class='account-link-container']//a[@href='/Hesabim/KullaniciBilgileri']"));
			act.moveToElement(elm).click().perform();
			driver.findElement(By.xpath("//input[@id='UserModel_FirstName']")).clear();
			driver.findElement(By.xpath("//input[@id='UserModel_FirstName']")).sendKeys(Name());
			driver.findElement(By.xpath("//input[@id='UserModel_LastName']")).clear();
			driver.findElement(By.xpath("//input[@id='UserModel_LastName']")).sendKeys(LastName());
			driver.findElement(By.xpath("//input[@id='UserModel_HiddenPhone']")).sendKeys(str);
			Select selectObject = new Select (driver.findElement(By.xpath("//select[@id='BirthDateDay']")));
			selectObject.selectByValue(sday);
			Thread.sleep(1000);
			Select selectObject2 = new Select (driver.findElement(By.xpath("//select[@id='BirthDateMonth']")));
			selectObject2.selectByValue(smonth);
			Thread.sleep(1000);
			Select selectObject3 = new Select (driver.findElement(By.xpath("//select[@id='BirthDateYear']")));
			selectObject3.selectByValue(sdate);
			Thread.sleep(1000);
			WebElement elm2=driver.findElement(By.xpath("//input[@id='User_Gender1' and @type='radio' and @value='Female']"));
			WebElement elm3=driver.findElement(By.xpath("//input[@id='User_Gender2' and @type='radio' and @value='Male']"));
			if(elm2.isSelected()) {
			act.moveToElement(elm3).click().perform();
			gender="Male";
			}else {
			act.moveToElement(elm2).click().perform();
			gender="Female";
			}
	
			Thread.sleep(1000);
			driver.findElement(By.xpath("//a[@class='updateBtn active']")).click();
			
		}
		@Test(priority=3, description="Checking Success Update Message")
		public void VerificationUpdate(){
			WebDriverWait wait = new WebDriverWait(driver,15);	
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@id='toast-container']//div[@class='toast toast-success']//div[@class='toast-message']")));
			//wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.xpath("//div[@class='toast-message']"))));
			System.out.println(driver.findElement(By.xpath("//div[@class='toast-message']")).getText());
			System.out.println("\n");
			
		}
		
		@Test(priority=4, description="Verification of User Informations")
		public void VerifyUserInformations () throws InterruptedException {
			
			Thread.sleep(2000);
			driver.findElement(By.xpath("//div[@id='headerMain']//a[@href='https://www.trendyol.com']")).click();
			Thread.sleep(1000);
			Actions act = new Actions(driver);
			WebElement element=driver.findElement(By.xpath("//div[@class='icon-container']//i[@class='icon navigation-icon-user']"));
			act.moveToElement(element).perform();
			Thread.sleep(1000);
			WebElement elm = driver.findElement(By.xpath("//div[@class='loggedin-panel-container']//div[@class='account-link-container']//a[@href='/Hesabim/KullaniciBilgileri']"));
			act.moveToElement(elm).click().perform();
			
			boolean comparisonName =driver.findElement(By.xpath("//input[@id='UserModel_FirstName']")).getAttribute("value").equals(Name());
			boolean comparisonLName =driver.findElement(By.xpath("//input[@id='UserModel_LastName']")).getAttribute("value").equals(LastName());
			String mobilesubstring = str.substring(Math.max(str.length() - 2, 0));
			String mobilesubstringonscreen=driver.findElement(By.xpath("//input[@id='UserModel_HiddenPhone']")).getAttribute("placeholder").substring(Math.max(str.length() - 2, 0));
			boolean comparisonMobile=mobilesubstringonscreen.equals(mobilesubstring);
			Select select2 = new Select (driver.findElement(By.xpath("//select[@id='BirthDateMonth']")));
			WebElement option = select2.getFirstSelectedOption();
			String defaultItem = option.getText();
			//System.out.println(defaultItem );
			boolean comparisonMonth =defaultItem.equals(smonth);
			Select select3 = new Select (driver.findElement(By.xpath("//select[@id='BirthDateYear']")));
			WebElement optionn = select3.getFirstSelectedOption();
			String defaultItemm = optionn.getText();
			boolean comparisonDate =defaultItemm.equals(sdate);
			//System.out.println(defaultItemm );
			boolean comparisonGender =driver.findElement(By.xpath("//input[@checked='checked' and @type='radio']")).getAttribute("value").equals(gender);
			
			System.out.println("Name comparison is " + comparisonName);
			System.out.println("Last Name comparison is " + comparisonLName);
			System.out.println("MobileNumber comparison is " + comparisonMobile);
			System.out.println("There is a defect about selected day. Concerned selected value is not listed in html ");
			System.out.println("Month comparison is " + comparisonMonth);
			System.out.println("Date comparison is " + comparisonDate);
			System.out.println("Gender comparison is " + comparisonGender);
			System.out.println("\n");
			
		}
		 public String Name () {
			 
				String str = String.valueOf(number);
		        String otherString = "Name";
		        otherString=otherString+str;
		        return otherString;
	  }
		 public String LastName () {
			
				String str = String.valueOf(number);
		        String otherString = "LastName";
		        otherString=otherString+str;
		        return otherString;
	}
}
